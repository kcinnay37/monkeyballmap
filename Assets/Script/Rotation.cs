using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotation : MonoBehaviour
{
    [SerializeField] float maxRotationSpeed = 5.0f;
    [SerializeField] float minRotationSpeed = 2.0f;

    float rotationSpeed;
    int direction;

    void Start()
    {
        rotationSpeed = Random.Range(minRotationSpeed, maxRotationSpeed);
        direction = Random.Range(0, 2);
    }

    void Update()
    {
        SetRotation();
    }

    void SetRotation()
    {
        float currentSpeed = 0.0f;
        if(direction == 1)
        {
            currentSpeed = rotationSpeed;
        }
        else
        {
            currentSpeed = -rotationSpeed;
        }
        transform.localEulerAngles += new Vector3(0.0f, 0.0f, currentSpeed);
    }
}
