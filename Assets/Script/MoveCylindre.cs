using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCylindre : MonoBehaviour
{
    [SerializeField] float rotationSpeed = 2.0f;
    [SerializeField] float movementSpeed = 2.0f;

    Rigidbody rb;
    Vector3 velo;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    
    void Update()
    {
        SetRotation();
        SetMovement();
    }

    void SetRotation()
    {
        transform.localEulerAngles += new Vector3(0.0f, 0.0f, rotationSpeed);
    }

    void SetMovement()
    {
        velo = rb.velocity;
        velo.x = movementSpeed;
        velo.z = 0;
        rb.velocity = velo;
    }
}
