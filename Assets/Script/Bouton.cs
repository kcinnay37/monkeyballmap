using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bouton : MonoBehaviour
{

    [SerializeField] GameObject plancher;
    [SerializeField] float distanceEnfonce;
    Vector3 position;
    
    void Start()
    {

    }

    
    void Update()
    {
        
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            BoutonPress();
        }
    }

    void BoutonPress()
    {
        Debug.Log("test");
        position = transform.position;
        position.x += distanceEnfonce;
        transform.position = position;
        plancher.SetActive(false);
    }
}
