using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBouce : MonoBehaviour
{
    [SerializeField] float forceBouce = 5;

    Rigidbody rb;
    Vector3 velo;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Bouce")
        {
            Bouce();
        }
    }

    void Bouce()
    {
        velo = rb.velocity;
        velo.y = forceBouce;
        rb.velocity = velo;
    }
}
